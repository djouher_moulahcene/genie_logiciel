package dj.umd.Deserial_group;

public class Personne_td {
	private String adress;
	private String nom;
	private String prenom;
	
	public Personne_td(String adress, String nom, String prenom) {
		super();
		this.adress = adress;
		this.nom = nom;
		this.prenom = prenom;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Personne_td [adress=" + adress + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	

}
