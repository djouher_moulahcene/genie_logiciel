package dj.umd.Deserial_group;


import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;






public class GestionTer {
	private ArrayList<Groupe> groupes1;//=new ArrayList<>();
	private ArrayList<Sujet> sujetsprop;
	
	public GestionTer() {
		super();
		this.groupes1=new ArrayList<>();
		this.sujetsprop=new ArrayList<>();	}
	
	public void addGroupe(String nom) {
		  this.groupes1.add(new Groupe(nom));
		}
		public void addSujet(String titre) {
		  this.sujetsprop.add(new Sujet(titre));
		}

	//le mieux c'est d'éviter de mettre une liste en parametres car il y'a risque de modéfication 
	public Groupe getGroupes(int i) {
		if (i>=1 && i<= groupes1.size()){
			return groupes1.get(i-1);
		}
		return null;
	}
	
	public int getNbGroupe() {
		  return this.groupes1.size();
		}
		public Groupe getGroupe(int id) {
		  if(id >= 0 && id < this.getNbGroupe()) {
		    return this.groupes1.get(id);
		  }
		  return new Groupe("n'existe pas");
		}
		
		
		public String toString() {
		  String str = "Liste des groupes:\n----------------\n";
		  for(int i=0; i< groupes1.size(); i++) {
		    str += groupes1.get(i).toString() + "\n";
		  }
		  str += "\n\nListe des sujets:\n------------------\n";
		  for(int i=0; i<this.sujetsprop.size(); i++) {
		    str += this.sujetsprop.get(i).toString() + "\n";
		  }
		  return str;
		}
		
		public static void main(String[] args) {
			
		  ObjectMapper test = new ObjectMapper();
		  
	
		
	/*
		ObjectMapper test1 = new ObjectMapper();
		  try {
		    test1.writeValue(new File("target/sujets.json"), this.sujetsprop);
		  } catch (JsonProcessingException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		  } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }
		}
		
		*/
		  try {
		  Groupe [] ges=test.readValue(new File("target/groupes.json"), 
					Groupe[].class);
		  
			for(int i=0; i<ges.length; i++) {
			System.out.println(ges[i]); }//ca appel implicitement la méthode toString 
		} catch (Exception e) {
			e.printStackTrace();
		
		
		/*
		  
		  try {
		    Sujet[] sujets = test1.readValue(new File("target/sujets.json"), Sujet[].class);
		    this.sujets = new ArrayList<Sujet>();
		    for(int i=0; i<sujets.length; i++) {
		      this.sujets.add(sujets[i]);
		    }
		  } catch (JsonMappingException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		  } catch (JsonProcessingException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		  } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		}
	

	
	
}	

}
