package dj.umd.Deserial_group;

public class Etudiant extends Personne_td {
	
	private int numEtu;

	public Etudiant(String adress, String nom, String prenom,int numEtu) {
		super(adress, nom, prenom);
		this.numEtu=numEtu;
		}

	public int getNumEtu() {
		return numEtu;
	}

	public void setNumEtu(int numEtu) {
		this.numEtu = numEtu;
	}

	@Override
	public String toString() {
		return "Etudiant [numEtu=" + numEtu + "]"+ super.toString();
	}
	
	

}
